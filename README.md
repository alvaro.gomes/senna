# Senna
Senna é um software de teste para testes de desenvolvimento em nodejs, express
e tecnologia afins.

# Ambiente de desenvolvimento
O ambiente com todas as ferramentas necessárias para desenvolvimento está
descrito na forma de uma imagem docker no arquivo Dockerfile-build. Para criar
seu ambiente basta executar:

    docker build -f Dockerfile-build -t build-hedge .

Para executar a aplicação dentro do conteiner de desenvolvimento basta
executar:

    docker run --rm -it -v $PWD:/code -p 3000:3000 build-hedge

Isso equivale a executar `npm start` na raiz de seu projeto. O script do npm a
ser executado pode ser substituído passando argumentos ao final do comando
acima. Por exemplo: antes de executar o projeto pela primeira vez é necessário
instalar as dependências com um `npm install` que equivaleria a:

    docker run --rm -it -v $PWD:/code -p 3000:3000 build-hedge install

