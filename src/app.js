var express = require('express');
var path = require('path');

var indexRouter = require('./controller/index');
var usersRouter = require('./controller/users');

var app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  console.log("Not found!");
  res.send('Not found!');
});

app.listen(process.env.PORT || '3000');
